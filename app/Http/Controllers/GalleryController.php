<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class GalleryController extends Controller
{
    public function indexPhoto()
    {
        return view('galleries.index-photo');
    }

    public function indexVideo()
    {
        $videos = Video::orderBy('created_at', 'dsc')->get();

        foreach ($videos as $video) {
            $url = $video->link;
            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
            $youtube_id = $match[1];
            $video->youtube_id = $youtube_id;
        }

        return view('galleries.index-video')->with(compact('videos'));
    }
}
