<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    // A post can have 1 author
	public function author() {
	    return $this->belongsTo('App\User','author_id');
	}
}
