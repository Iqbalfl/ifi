@extends('layouts.app')
@section('content') 
  <!-- Hero Section-->
  <section style="background: url({{ asset('/vendor/ifi/img') . '/hero.jpg' }}); background-size: cover; background-position: center center" class="hero">
    <div class="container">
      <div class="row">
        <div class="col-lg-7">
          <h1>Welcome to Islamic Fashion Institute</h1><a href="{{ url('/page/about') }}" class="hero-link">Discover More</a>
        </div>
      </div><a href=".intro" class="continue link-scroll"><i class="fa fa-long-arrow-down"></i> Scroll Down</a>
    </div>
  </section>
  <!-- Intro Section-->
  <section class="intro">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <h2 class="h3">IFI – ISLAMIC FASHION INSTITUTE</h2>
          <p class="text-big">
            <p style="text-align: justify;">Sekolah Fashion Islam pertama di Indonesia Kurikulum berbasis Standar Kompetensi Kerja Nasional Indonesia ( SKKNI) yang spesifik berpedoman pada kaidah-kaidah Islam. Kurikulum tersebut akan diterapkan pada pengajaran perancangan busana muslim, pergaulan, dan bisnis</p>
            <p>Sehingga IFI melahirkan lulusan yang :</p>
            <p>1. Santun dalam pergaulan<br />2. Beretika dalam berbisnis<br />3. Jujur dalam berkarya<br />4. Menghargai hak cipta<br />5. Menjalankan Ukhuwah Islamiyah</p>
            <p style="text-align: justify;">VISI<br />&bull; Menjadi pusat industri kreatif khususnya di busana muslim yang tumbuh dan berkembang, dalam upaya menjadikan Indonesia sebagai kiblat busana muslim dunia.<br />&bull; Islamic Fashion Institute dalam perkembangannya akan diarahkan sebagai pusat inkubasi bisnis</p>
            </p>
        </div>
      </div>
    </div>
  </section>
  <!-- Divider Section-->
  <section style="background: rgba(0, 0, 0, 0) url(&quot;/vendor/ifi/img/divider-bg.jpg&quot;) repeat scroll left 0px / cover;" class="divider">
    <div class="container">
      <div class="row">
        <div class="col-md-7">
          IFI<br>
            <h2>ISLAMIC FASHION INSTITUTE</h2>
        </div>
      </div>
    </div>
  </section>
  <!-- Latest Posts -->
  <section class="latest-posts"> 
    <div class="container">
      <header> 
        <h2>Latest from the IFI</h2>
        <p class="text-big">Informasi terbaru dari website IFI</p>
      </header>
      @include('partials._latest_3-post')
    </div>
  </section>
  <!-- Newsletter Section-->
  <section class="newsletter no-padding-top">    
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2>Subscribe IFI</h2>
          <p class="text-big">Masukan email anda dan dapatkan informasi terbaru dari IFI</p>
        </div>
        <div class="col-md-8">
          <div class="form-holder">
            <form action="#">
              <div class="form-group">
                <input name="email" id="email" placeholder="Type your email address" type="email">
                <button type="submit" class="submit">Subscribe</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Gallery Section-->
  <section class="gallery no-padding">    
    <div class="row">
      <div class="mix col-lg-3 col-md-3 col-sm-6">
        <div class="item"><a href="/vendor/ifi/img/port01.jpg" data-fancybox="gallery" class="image"><img src="/vendor/ifi/img/port01.jpg" alt="..." class="img-fluid">
            <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div></a></div>
      </div>
      <div class="mix col-lg-3 col-md-3 col-sm-6">
        <div class="item"><a href="/vendor/ifi/img/port02.jpg" data-fancybox="gallery" class="image"><img src="/vendor/ifi/img/port02.jpg" alt="..." class="img-fluid">
            <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div></a></div>
      </div>
      <div class="mix col-lg-3 col-md-3 col-sm-6">
        <div class="item"><a href="/vendor/ifi/img/port03.jpg" data-fancybox="gallery" class="image"><img src="/vendor/ifi/img/port03.jpg" alt="..." class="img-fluid">
            <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div></a></div>
      </div>
      <div class="mix col-lg-3 col-md-3 col-sm-6">
        <div class="item"><a href="/vendor/ifi/img/port04.jpg" data-fancybox="gallery" class="image"><img src="/vendor/ifi/img/port04.jpg" alt="..." class="img-fluid">
            <div class="overlay d-flex align-items-center justify-content-center"><i class="icon-search"></i></div></a></div>
      </div>
    </div>
  </section>
@endsection