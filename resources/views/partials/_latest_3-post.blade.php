<div class="row">
  @forelse (App\Post::latestPosts()->limit(3)->get() as $latpost)
    <div class="post col-md-4">
      <div class="post-thumbnail"><a href="{{ url('post',$latpost->slug) }}"><img src="{{ Voyager::image($latpost->image) }}" alt="..." class="img-fluid"></a></div>
      <div class="post-details">
        <div class="post-meta d-flex justify-content-between">
          <div class="date">{{ $latpost->created_at->format('d M | Y')}}</div>
          <div class="category"><a href="{{ url('/post?category='.$latpost->category->slug) }}">{{ $latpost->category->slug }}</a></div>
        </div><a href="post.html">
          <h3 class="h4">{{ $latpost->title }}</h3></a>
          <p class="text-muted">
            {{ str_limit($latpost->excerpt, 250 )}}
          </p>
      </div>
    </div>
  @empty
    Belum Ada Artikel
  @endforelse
</div>

