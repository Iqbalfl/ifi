@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12 mt-4"><center><h2>Galleri Video - IFI</h2></center></div>
      <!-- Gallery Section-->
        @foreach ($videos as $item)
            <div class="col-md-6 mt-4">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">{{str_limit($item->title, 43)}}</h5>
                  <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{$item->youtube_id}}?rel=0" allowfullscreen></iframe>
                  </div> 
                  <p></p>
                  <p class="card-text">{{$item->description}}</p>
                </div>
              </div>
            </div>
        @endforeach
      <div class="col-md-12 mb-4">{{-- separate --}}</div>
    </div>
  </div>
@endsection