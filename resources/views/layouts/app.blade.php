<!DOCTYPE html>
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="/vendor/ifi/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="/vendor/ifi/css/font-awesome.min.css">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="/vendor/ifi/css/fontastic.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <!-- Fancybox-->
    <link rel="stylesheet" href="/vendor/ifi/css/jquery.fancybox.min.css.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="/vendor/ifi/css/style.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="/vendor/ifi/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="/vendor/ifi/img/logo.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="animsition">
      <header class="header">
        <!-- Main Navbar-->
        <nav class="navbar fixed-top navbar-expand-lg">
          <div class="search-area">
            <div class="search-area-inner d-flex align-items-center justify-content-center">
              <div class="close-btn"><i class="icon-close"></i></div>
              <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                  <form action="#">
                    <div class="form-group">
                      <input name="search" id="search" placeholder="Apa yang anda cari?" type="search">
                      <button type="submit" class="submit"><i class="icon-search-1"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="container">
            <!-- Navbar Brand -->
            <div class="navbar-header d-flex align-items-center justify-content-between">
               <a class="navbar-brand logo" href="{{ url('/') }}">
                <img src="/vendor/ifi/img/logo.png" class="d-inline-block logo" alt="">
                Islamic Fashion Institute
               </a>
              <!-- Navbar Brand -->
              <!-- Toggle Button-->
              <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
            </div>
            <!-- Navbar Menu -->
            <div id="navbarcollapse" class="collapse navbar-collapse">
              {{ menu('web','partials._main-menu') }}
              <div class="navbar-text"><a href="#" class="search-btn"><i class="icon-search-1"></i></a></div>
              <ul class="langs navbar-text"><a href="#" class="active">ID</a></ul>
            </div>
          </div>
        </nav>
      </header>

      @yield('content')
      
     <!-- Page Footer-->
      <footer class="main-footer">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="logo">
                <h6 class="text-white">Contact Us</h6>
              </div>
              <div class="contact-details">
                <p><i class="fa fa-map-marker fa-fw"></i> Jl. Naripan No.89, Kb. Pisang<br>
                  <i class="fa fa-fw"></i> Sumur Bandung, Kota Bandung<br>
                  <i class="fa fa-fw"></i> Jawa Barat | 40112</p>
                <p><i class="fa fa-phone fa-fw"></i> Phone : (+62) 897-7395-630</p>
                <p><i class="fa fa-envelope fa-fw"></i> Email : <a href="mailto:islamicfashion40@gmail.com">islamicfashion40@gmail.com</a></p>
                <ul class="social-menu">
                  <li class="list-inline-item"><a href="https://www.facebook.com/islamicfashioninstitute"><i class="fa fa-facebook"></i></a></li>
                  <li class="list-inline-item"><a href="https://twitter.com/ifibandung"><i class="fa fa-twitter"></i></a></li>
                  <li class="list-inline-item"><a href="https://www.instagram.com/islamicfashioninstitute"><i class="fa fa-instagram"></i></a></li>
                  <li class="list-inline-item"><a href="https://www.youtube.com/channel/UCBlAoixpdaElEjA-uoZjc_Q"><i class="fa fa-youtube"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-4">
              <div class="latest-posts">
                <div class="latest-posts"><a href="/">
                  <div class="post d-flex align-items-center">
                    <div class="image"><img src="/vendor/ifi/img/logo.png" alt="..." class="img-fluid"></div>
                    <div class="title"><strong>IFI</strong><span class="date last-meta">Islamic Fashion Institute</span></div>
                  </div></a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="logo">
                <h6 class="text-white">Link Alternatif</h6>
              </div>
              <div class="menus d-flex">
                <ul class="list-unstyled">
                  <li> <a href="{{ url('http://sisfo.islamicfashioninstitute.or.id/login') }}" target="blank">Login</a></li>
                  <li> <a href="{{ url('http://sisfo.islamicfashioninstitute.or.id/register') }}" target="blank">Registrasi</a></li>
                  <li> <a href="#" target="blank">Info Pendaftaran</a></li>
                  <li> <a href="{{ url('/page/kebijakan-privasi') }}" target="blank">Kebijakan Privasi</a></li>
                </ul>
              </div>
            </div>       
          </div>
        </div>
        <div class="copyrights">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <p>© {{date('Y')}}. All rights reserved. Islamic Fashion Iinstitute.</p>
              </div>
              <div class="col-md-6 text-right">
                <p>Made with <i class="fa fa-heart"></i> in Bandung - Indonesia
                </p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
    
    <!-- Javascript files-->
    <script src="/vendor/ifi/js/jquery-3.2.1.min.js"></script>
    <script src="/vendor/ifi/js/popper.min.js"> </script>
    <script src="/vendor/ifi/js/bootstrap.min.js"></script>
    <script src="/vendor/ifi/js/jquery.cookie.js"> </script>
    <script src="/vendor/ifi/js/jquery.fancybox.min.js"></script>
    <script src="/vendor/ifi/js/front.js"></script>
    <!-- disqus count js -->
    <script id="dsq-count-scr" src="//islamic-fashion-institute.disqus.com/count.js" async></script>
</body>
</html>