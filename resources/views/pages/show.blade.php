@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <!-- Latest Posts -->
      <main class="post blog-post col-lg-8"> 
        <div class="container">
          <div class="post-single">
            <div class="post-details">
              <div class="post-meta d-flex justify-content-between">
                <div class="category"><a href="#"></a></div>
              </div>
              <h1>{{ $page->title }}</h1>
              <div class="post-body">
                {!! $page->body !!}
              </div>
              {{-- <div class="post-tags">
                <a href="#" class="tag">#Kursus</a>
                <a href="#" class="tag">#IFI</a>
                <a href="#" class="tag">#Islamic</a>
                <a href="#" class="tag">#Fashion</a>
              </div> --}}
            </div>
          </div>
        </div>
      </main>
      <aside class="col-lg-4">
      </aside>
    </div>
  </div>
@endsection